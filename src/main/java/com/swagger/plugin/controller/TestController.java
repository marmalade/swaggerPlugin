package com.swagger.plugin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.taikang.dic.ltci.api.model.ResultDTO;

@Controller
@RequestMapping(path = "/api/v1", produces = "application/json;charset=utf-8")
public class TestController {

	@RequestMapping(path = "/test/getName", method = RequestMethod.GET)
	@ResponseBody
	public ResultDTO getInHospState(String name) {
		ResultDTO resultDTO = new ResultDTO();
		resultDTO.setDatas(name);
		return resultDTO;
	}

}
